<?php
/*
Plugin Name: Test GitLab Plugin
Plugin URI: https://gitlab.com/afragen/test-gitlab-plugin/
Description: This plugin is used for testing functionality of Github updating of plugins.
Version: 1.0
Author: Andy Fragen
License: GNU General Public License v2
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
GitLab Plugin URI: https://gitlab.com/afragen/test-gitlab-plugin/
Requires PHP: 5.4
*/